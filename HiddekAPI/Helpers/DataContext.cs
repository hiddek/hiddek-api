using Microsoft.EntityFrameworkCore;
using HiddekAPI.Entities;

namespace HiddekAPI.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
    }
}
