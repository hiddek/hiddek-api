using AutoMapper;
using HiddekAPI.Dtos;
using HiddekAPI.Entities;

namespace HiddekAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}